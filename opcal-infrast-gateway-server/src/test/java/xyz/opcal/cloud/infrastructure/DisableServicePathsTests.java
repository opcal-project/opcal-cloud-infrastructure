/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import xyz.opcal.cloud.commons.web.WebConstants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = { "dev", "experiment" })
class DisableServicePathsTests {

	static final String LOAD_CONFIG_URL = "/config-server/unit-test/dev/local";
	static final String HEALTH_URL = "/config-server/actuator/health";

	private @Autowired WebTestClient webTestClient;

	@Value("${experiment.config.username}")
	private String username;

	@Value("${experiment.config.password}")
	private String password;

	@Test
	void allow() {

		WebTestClient newTestClient = webTestClient.mutate().responseTimeout(Duration.ofSeconds(60)).build();
		newTestClient.get().uri(LOAD_CONFIG_URL) //
				.headers(httpHeaders -> httpHeaders.setBasicAuth(username, password)) //
				.exchange().expectStatus().isOk();
		newTestClient.get().uri(LOAD_CONFIG_URL) //
				.headers(httpHeaders -> httpHeaders.setBasicAuth(username, password)) //
				.header(WebConstants.HEADER_X_REAL_IP, "1.2.3.4") //
				.exchange().expectStatus().isOk();
	}

	@Test
	void disallow() {
		webTestClient.get().uri(HEALTH_URL) //
				.headers(httpHeaders -> httpHeaders.setBasicAuth(username, password)) //
				.exchange().expectStatus().isNotFound();
		webTestClient.get().uri(HEALTH_URL) //
				.headers(httpHeaders -> httpHeaders.setBasicAuth(username, password)) //
				.header(WebConstants.HEADER_X_REAL_IP, "11.12.13.14") //
				.exchange().expectStatus().isNotFound();
	}
}
