/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.reactive.server.WebTestClient;

import xyz.opcal.cloud.commons.web.WebConstants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AllowIpPathTests {

	private @Autowired WebTestClient webTestClient;

	@Test
	void allow() {
		webTestClient.get().uri("/actuator/health").exchange().expectStatus().isOk();
		webTestClient.get().uri("/actuator/health") //
				.header(WebConstants.HEADER_CF_CONNECTING_IP, "192.168.90.100") //
				.exchange().expectStatus().isOk();

		webTestClient.get().uri("/actuator/beans") //
				.header(WebConstants.HEADER_X_REAL_IP, "10.2.3.5") //
				.exchange().expectStatus().isOk();
	}

	@Test
	void disallow() {
		webTestClient.get().uri("/actuator/health") //
				.header(WebConstants.HEADER_CF_CONNECTING_IP, "192.17.90.100") //
				.exchange().expectStatus().isForbidden();

		webTestClient.get().uri("/actuator/beans") //
				.header(WebConstants.HEADER_X_REAL_IP, "1.2.3.4") //
				.exchange().expectStatus().isForbidden();
	}
}
