/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.gateway.filter.web;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.util.SubnetUtils;
import org.springframework.cloud.gateway.support.HttpStatusHolder;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import xyz.opcal.cloud.commons.web.utils.ServerHttpRequestUtils;
import xyz.opcal.cloud.infrastructure.gateway.filter.web.config.AllowIpPathConfig;

public class AllowIpPathFilter implements WebFilter {

	private final AntPathMatcher pathMatcher = new AntPathMatcher();
	private final List<SubnetUtils> allowCidrs = new ArrayList<>();
	private final List<String> allowIps = new ArrayList<>();
	private AllowIpPathConfig allowIpPathConfig;

	public AllowIpPathFilter(AllowIpPathConfig allowIpPathConfig) {
		this.allowIpPathConfig = allowIpPathConfig;
		init(this.allowIpPathConfig);
	}

	protected void init(AllowIpPathConfig allowIpPathConfig) {
		allowIpPathConfig.getIps().stream().filter(s -> StringUtils.contains(s, "/")).map(SubnetUtils::new).forEach(this.allowCidrs::add);
		allowIpPathConfig.getIps().stream().filter(s -> StringUtils.containsNone(s, "/")).forEach(this.allowIps::add);
	}

	private boolean matchRequest(ServerHttpRequest request) {
		return this.allowIpPathConfig.getPaths().stream().anyMatch(pattern -> pathMatcher.match(pattern, request.getPath().value()));
	}

	private boolean allowIp(String ip) {
		return allowIps.stream().anyMatch(s -> StringUtils.equals(s, ip)) || allowCidrs.stream().map(SubnetUtils::getInfo).anyMatch(info -> info.isInRange(ip));
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		if (matchRequest(exchange.getRequest()) && !allowIp(ServerHttpRequestUtils.getIp(exchange.getRequest()))) {
			HttpStatusHolder httpStatusHolder = HttpStatusHolder.parse(allowIpPathConfig.getDisallowStatus());
			exchange.getResponse().setRawStatusCode(HttpStatus.NOT_FOUND.value());
			ServerWebExchangeUtils.setResponseStatus(exchange, httpStatusHolder);
			return Mono.empty();
		}
		return chain.filter(exchange);
	}

}
