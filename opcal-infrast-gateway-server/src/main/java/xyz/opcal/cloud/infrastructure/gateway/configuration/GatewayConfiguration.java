/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.gateway.configuration;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.observation.ServerRequestObservationContext;
import org.springframework.web.server.WebFilter;

import io.micrometer.common.KeyValue;
import io.micrometer.observation.Observation;
import io.micrometer.observation.Observation.Context;
import io.micrometer.observation.ObservationFilter;
import io.micrometer.tracing.exporter.SpanExportingPredicate;
import xyz.opcal.cloud.commons.autoconfigure.annotation.EnableOpcalCloud;
import xyz.opcal.cloud.commons.logback.webflux.annotation.EnableLogWebfluxRequestId;
import xyz.opcal.cloud.commons.web.WebConstants;
import xyz.opcal.cloud.commons.web.context.RequestThreadContext;
import xyz.opcal.cloud.infrastructure.gateway.filter.factory.DisableServicePatternsGatewayFilterFactory;
import xyz.opcal.cloud.infrastructure.gateway.filter.web.AllowIpPathFilter;
import xyz.opcal.cloud.infrastructure.gateway.filter.web.config.AllowIpPathConfig;
import xyz.opcal.cloud.infrastructure.gateway.tracing.GatewayRequestObservationConvention;

@Configuration
@EnableLogWebfluxRequestId
@EnableOpcalCloud
@EnableConfigurationProperties(value = { AllowIpPathConfig.class })
public class GatewayConfiguration {

	@Bean
	public DisableServicePatternsGatewayFilterFactory disableServicePatternsGatewayFilterFactory() {
		return new DisableServicePatternsGatewayFilterFactory();
	}

	@Bean
	public AllowIpPathFilter allowIpPathFilter(AllowIpPathConfig allowIpPathConfig) {
		return new AllowIpPathFilter(allowIpPathConfig);
	}

	@Bean
	ObservationFilter urlObservationFilter() {
		return context -> {
			if (StringUtils.startsWith(context.getName(), "spring.security.")) {
				Context root = getRoot(context);
				if (StringUtils.equals(root.getName(), "http.server.requests")) {
					context.addHighCardinalityKeyValue(root.getHighCardinalityKeyValue("http.url"));
				}
			}
			return context;
		};
	}

	private Observation.Context getRoot(Observation.Context context) {
		if (context.getParentObservation() == null) {
			return context;
		} else {
			return getRoot((Context) context.getParentObservation().getContextView());
		}
	}

	@Bean
	SpanExportingPredicate actuatorSpanExportingPredicate() {
		return span -> !StringUtils.startsWith(span.getTags().get("http.url"), "/actuator");
	}

	/**
	 * observation for server
	 * 
	 * @return
	 */
	@Bean
	WebFilter webfluxRequestObservationFilter() {
		return (exchange, chain) -> {
			ServerRequestObservationContext.findCurrent(exchange.getAttributes()).ifPresent(context -> context.addHighCardinalityKeyValue(
					KeyValue.of(WebConstants.HEADER_X_REQUEST_ID.toLowerCase(), (String) RequestThreadContext.get(WebConstants.HEADER_X_REQUEST_ID))));
			return chain.filter(exchange);
		};
	}

	/**
	 * observation for gateway routes
	 * 
	 * @return
	 */
	@Bean
	GatewayRequestObservationConvention requestDetailObservationConvention() {
		return new GatewayRequestObservationConvention();
	}

}
