/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.gateway.filter.factory;

import static org.springframework.cloud.gateway.support.GatewayToStringStyler.filterToStringCreator;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.support.HttpStatusHolder;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import lombok.Getter;
import lombok.Setter;
import reactor.core.publisher.Mono;

public class DisableServicePatternsGatewayFilterFactory extends AbstractGatewayFilterFactory<DisableServicePatternsGatewayFilterFactory.PatternsConfig> {

	public static final String PATTERN_KEY = "pattern";
	public static final String STATUS_KEY = "status";

	private final AntPathMatcher pathMatcher = new AntPathMatcher();

	public DisableServicePatternsGatewayFilterFactory() {
		super(PatternsConfig.class);
	}

	@Override
	public List<String> shortcutFieldOrder() {
		return Arrays.asList(PATTERN_KEY, STATUS_KEY);
	}

	@Override
	public GatewayFilter apply(PatternsConfig config) {
		return new GatewayFilter() {
			@Override
			public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
				if (matchRequestPath(config, exchange.getRequest())) {
					HttpStatusHolder httpStatusHolder = HttpStatusHolder.parse(config.status);
					exchange.getResponse().setRawStatusCode(HttpStatus.NOT_FOUND.value());
					ServerWebExchangeUtils.setResponseStatus(exchange, httpStatusHolder);
					return Mono.empty();
				}
				return chain.filter(exchange);
			}

			@Override
			public String toString() {
				return filterToStringCreator(DisableServicePatternsGatewayFilterFactory.this) //
						.append(PATTERN_KEY, config.getPattern()) //
						.append(STATUS_KEY, config.getStatus()) //
						.toString();
			}
		};
	}

	private boolean matchRequestPath(PatternsConfig config, ServerHttpRequest request) {
		return Arrays.stream(StringUtils.split(config.getPattern(), "|")).anyMatch(pattern -> pathMatcher.match(pattern, request.getPath().value()));
	}

	@Getter
	@Setter
	public static class PatternsConfig {
		private String pattern;
		private String status;
	}

}
