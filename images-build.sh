#!/bin/bash

#export TAG_VERSION=v${PROJECT_VERSION}-${BUILD_NUMBER}
#export LATEST_VERSION=v${PROJECT_VERSION}

echo "current build tag ${TAG_VERSION}"

#cp /tmp/apt/apt.conf ./

chmod +x ./ci/builds/**.sh

find ${WS_PATH}/ci/builds/ -type f -iname '*.sh' | sort -n | xargs -I {} bash {};