# opcal-cloud-infrastructure

[![Build Status](https://jnx.opcal.xyz/buildStatus/icon?job=opcal-cloud-infrastructure)](http://jenkins.opcal.xyz/job/opcal-cloud-infrastructure/)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=opcal-project_opcal-cloud-infrastructure&metric=alert_status)](https://sonarcloud.io/dashboard?id=opcal-project_opcal-cloud-infrastructure)
[![codecov](https://codecov.io/gl/opcal-project/opcal-cloud-commons/branch/main/graph/badge.svg?token=AEBJ3Z5AJX)](https://codecov.io/gl/opcal-project/opcal-cloud-commons)

infrastructure system app
