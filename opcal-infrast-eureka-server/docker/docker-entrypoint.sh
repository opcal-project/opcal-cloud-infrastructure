#!/bin/bash

set -e

JAVA_OPTS="$JAVA_OPTS"

# Allow the container to be started with `--user`
if [[ "$1" = 'java' && "$(id -u)" = '0' ]]; then
  chown -R opcal:opcal "/app"
  exec gosu opcal "$0" "$@"
fi

exec java ${JAVA_OPTS} -jar "$@" ${PARAMS}