#!/bin/bash

IMAGE_TAGS=("${LATEST_VERSION}" "${BRANCH_TAG}")

if [ ${releaseBuild} == 'true' ]; then
  IMAGE_TAGS=(${IMAGE_TAGS[@]} "${BRANCH_RELEASE}")

  if [ ${latestBuild} == 'true' ]; then
    IMAGE_TAGS=(${IMAGE_TAGS[@]} "latest")
  fi
else
  IMAGE_TAGS=(${IMAGE_TAGS[@]} "${TAG_VERSION}")
  if [ ${BRANCH} == 'main' ]; then
    IMAGE_TAGS=(${IMAGE_TAGS[@]} "SNAPSHOT")
  fi
fi

IMAGE_REPO="${REGISTRY_URL}/opcal-project/opcal-cloud-infrastructure/opcal-infrast-config-server"

BUILD_TAGS=""
for itag in "${IMAGE_TAGS[@]}"
do
  BUILD_TAGS=" ${BUILD_TAGS} -t ${IMAGE_REPO}:${itag} "
done

docker build \
  ${BUILD_TAGS} \
  -f ${WS_PATH}/opcal-infrast-config-server/docker/Dockerfile . --no-cache

for itag in "${IMAGE_TAGS[@]}"
do
  docker push ${IMAGE_REPO}:${itag}
  docker rmi -f ${IMAGE_REPO}:${itag}
done

echo "build opcal-infrast-config-server finished"