/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.util.SubnetUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import xyz.opcal.cloud.commons.web.utils.HttpServletRequestUtils;
import xyz.opcal.cloud.infrastructure.OpcalConfigServerConstants;
import xyz.opcal.cloud.infrastructure.http.auth.AllowIpAuthenticationToken;
import xyz.opcal.cloud.infrastructure.http.auth.GitlabAuthenticationToken;
import xyz.opcal.cloud.infrastructure.http.auth.config.AuthenticationConfig;

public class ConfigServerAuthenticateFilter extends OncePerRequestFilter {

	private final AuthenticationConfig authenticationConfig;
	private final List<AntPathRequestMatcher> allowIpPathMatchers = new ArrayList<>();
	private final List<SubnetUtils> allowCidrs = new ArrayList<>();
	private final List<String> allowIps = new ArrayList<>();

	public ConfigServerAuthenticateFilter(AuthenticationConfig monitorConfig) {
		this.authenticationConfig = monitorConfig;
		init(this.authenticationConfig);
	}

	protected void init(AuthenticationConfig authenticationConfig) {
		authenticationConfig.getAllowIpPaths().stream().map(AntPathRequestMatcher::new).forEach(this.allowIpPathMatchers::add);
		authenticationConfig.getAllowIps().stream().filter(s -> StringUtils.contains(s, "/")).map(SubnetUtils::new).forEach(this.allowCidrs::add);
		authenticationConfig.getAllowIps().stream().filter(s -> StringUtils.containsNone(s, "/")).forEach(this.allowIps::add);
	}

	private boolean matchRequest(HttpServletRequest request) {
		return allowIpPathMatchers.stream().anyMatch(matcher -> matcher.matches(request));
	}

	private boolean allowIp(String ip) {
		return allowIps.stream().anyMatch(s -> StringUtils.equals(s, ip)) || allowCidrs.stream().map(SubnetUtils::getInfo).anyMatch(info -> info.isInRange(ip));
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		if (matchRequest(request)) {
			String ip = HttpServletRequestUtils.getIp(request);
			if (allowIp(ip)) {
				SecurityContextHolder.getContext().setAuthentication(new AllowIpAuthenticationToken(ip));
			}
		} else {
			String token = HttpServletRequestUtils.cleanHeaderTaint(request, OpcalConfigServerConstants.HEADER_GITLAB_TOKEN);
			if (CollectionUtils.contains(Collections.enumeration(authenticationConfig.getTokens()), token)) {
				SecurityContextHolder.getContext().setAuthentication(new GitlabAuthenticationToken(token));
			}
		}
		filterChain.doFilter(request, response);
	}

}
