/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.configuration;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import io.micrometer.observation.Observation;
import io.micrometer.observation.Observation.Context;
import io.micrometer.observation.ObservationFilter;
import io.micrometer.tracing.exporter.SpanExportingPredicate;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.commons.autoconfigure.annotation.EnableOpcalCloud;
import xyz.opcal.cloud.infrastructure.http.ConfigServerAuthenticateFilter;
import xyz.opcal.cloud.infrastructure.http.auth.config.AuthenticationConfig;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableOpcalCloud
@EnableConfigurationProperties(value = { AuthenticationConfig.class })
public class ConfigServerConfiguration {

	private static final String NOOP_PASSWORD_PREFIX = "{noop}";

	private static final Pattern PASSWORD_ALGORITHM_PATTERN = Pattern.compile("^\\{.+}.*$");

	@Bean
	public InMemoryUserDetailsManager inMemoryUserDetailsManager(SecurityProperties properties, AuthenticationConfig authenticationConfig,
			ObjectProvider<PasswordEncoder> passwordEncoder) {
		var users = new ArrayList<UserDetails>();
		var defaultUser = createUserDetails(properties.getUser(), passwordEncoder);
		users.add(defaultUser);
		// @formatter:off
		authenticationConfig.getUsers().stream()
				.filter(user -> !org.apache.commons.lang3.StringUtils.equals(defaultUser.getUsername(), user.getName()))
				.map(user -> createUserDetails(user, passwordEncoder))
				.forEach(users::add);
		// @formatter:on
		return new InMemoryUserDetailsManager(users.toArray(UserDetails[]::new));
	}

	private UserDetails createUserDetails(SecurityProperties.User user, ObjectProvider<PasswordEncoder> passwordEncoder) {
		return User.withUsername(user.getName()).password(getOrDeducePassword(user, passwordEncoder.getIfAvailable()))
				.roles(StringUtils.toStringArray(user.getRoles())).build();
	}

	private String getOrDeducePassword(SecurityProperties.User user, PasswordEncoder encoder) {
		String password = user.getPassword();
		if (user.isPasswordGenerated()) {
			// @formatter:off
			log.warn(String.format(
					"%n%nUsing generated security password: %s%n%nThis generated password is for development use only. "
							+ "Your security configuration must be updated before running your application in "
							+ "production.%n",
					user.getPassword()));
			// @formatter:on
		}
		if (encoder != null || PASSWORD_ALGORITHM_PATTERN.matcher(password).matches()) {
			return password;
		}
		return NOOP_PASSWORD_PREFIX + password;
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationConfig authenticationConfig) throws Exception {
		// @formatter:off
		http.csrf(CsrfConfigurer::disable)
				.httpBasic(Customizer.withDefaults())
				.addFilterBefore(new ConfigServerAuthenticateFilter(authenticationConfig), BasicAuthenticationFilter.class) //
				.authorizeHttpRequests(customizer -> customizer.anyRequest().authenticated());
		// @formatter:on
		return http.build();
	}

	@Bean
	ObservationFilter urlObservationFilter() {
		return context -> {
			if (org.apache.commons.lang3.StringUtils.startsWith(context.getName(), "spring.security.")) {
				Context root = getRoot(context);
				if (org.apache.commons.lang3.StringUtils.equals(root.getName(), "http.server.requests")) {
					context.addHighCardinalityKeyValue(root.getHighCardinalityKeyValue("http.url"));
				}
			}
			return context;
		};
	}

	private Observation.Context getRoot(Observation.Context context) {
		if (context.getParentObservation() == null) {
			return context;
		} else {
			return getRoot((Context) context.getParentObservation().getContextView());
		}
	}

	@Bean
	SpanExportingPredicate actuatorSpanExportingPredicate() {
		return span -> !org.apache.commons.lang3.StringUtils.startsWith(span.getTags().get("http.url"), "/actuator");
	}

}
