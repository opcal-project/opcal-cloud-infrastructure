/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.http.auth.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(AuthenticationConfig.PREFIX)
@RefreshScope
public class AuthenticationConfig {

	public static final String PREFIX = "opcal.cloud.config.server.authentication";
	private Set<String> tokens;
	private Set<String> allowIps;
	private Set<String> allowIpPaths;

	private List<SecurityProperties.User> users = new ArrayList<>();

}
