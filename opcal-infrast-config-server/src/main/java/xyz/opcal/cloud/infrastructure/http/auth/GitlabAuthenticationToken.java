/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure.http.auth;

import java.util.Collections;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import xyz.opcal.cloud.infrastructure.OpcalConfigServerConstants;

public class GitlabAuthenticationToken extends AbstractAuthenticationToken {

	private final String token;

	public GitlabAuthenticationToken(String token) {
		super(Collections.emptyList());
		this.token = token;
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return token;
	}

	@Override
	public Object getPrincipal() {
		return OpcalConfigServerConstants.HEADER_GITLAB_TOKEN;
	}
}
