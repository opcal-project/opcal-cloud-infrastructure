/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;

import xyz.opcal.cloud.infrastructure.http.auth.config.AuthenticationConfig;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class GitlabTokenTests {

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired AuthenticationConfig authenticationConfig;

	@Test
	void withToken() throws IOException {

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		authenticationConfig.getTokens().stream().findFirst().ifPresent(token -> headers.add(OpcalConfigServerConstants.HEADER_GITLAB_TOKEN, token));
		String body = IOUtils.toString(ResourceUtils.getURL("classpath:monitor.json"), StandardCharsets.UTF_8);
		HttpEntity<String> request = new HttpEntity<>(body, headers);
		ResponseEntity<String> result = testRestTemplate.postForEntity("/monitor", request, String.class);
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	void withoutToken() throws IOException {

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		String body = IOUtils.toString(ResourceUtils.getURL("classpath:monitor.json"), StandardCharsets.UTF_8);
		HttpEntity<String> request = new HttpEntity<>(body, headers);
		ResponseEntity<String> result = testRestTemplate.postForEntity("/monitor", request, String.class);
		assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());

	}

}
