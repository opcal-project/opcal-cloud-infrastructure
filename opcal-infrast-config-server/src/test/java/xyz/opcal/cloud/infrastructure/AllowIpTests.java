/*
 * Copyright 2021-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.cloud.infrastructure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AllowIpTests {

	private @Autowired TestRestTemplate testRestTemplate;

	@Test
	void allow() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Real-Ip", "9.8.7.6");
		RequestEntity<String> request1 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result1 = testRestTemplate.exchange(request1, String.class);
		assertEquals(HttpStatus.OK, result1.getStatusCode());

		headers.add("X-Real-Ip", "10.0.1.3");
		RequestEntity<String> request2 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result2 = testRestTemplate.exchange(request2, String.class);
		assertEquals(HttpStatus.OK, result2.getStatusCode());

		headers.add("X-Real-Ip", "10.2.3.1");
		RequestEntity<String> request3 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result3 = testRestTemplate.exchange(request3, String.class);
		assertEquals(HttpStatus.OK, result3.getStatusCode());

		headers.add("X-Real-Ip", "172.18.3.1");
		RequestEntity<String> request4 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result4 = testRestTemplate.exchange(request4, String.class);
		assertEquals(HttpStatus.OK, result4.getStatusCode());
	}

	@Test
	void disallow() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Real-Ip", "18.9.34.78");
		RequestEntity<String> request1 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result1 = testRestTemplate.exchange(request1, String.class);
		assertEquals(HttpStatus.UNAUTHORIZED, result1.getStatusCode());

		headers.add("X-Real-Ip", "172.32.1.0");
		RequestEntity<String> request2 = new RequestEntity<>(headers, HttpMethod.GET, URI.create("/actuator/info"));
		ResponseEntity<String> result2 = testRestTemplate.exchange(request2, String.class);
		assertEquals(HttpStatus.UNAUTHORIZED, result2.getStatusCode());
	}
}
