#!/bin/bash

set -e

JAVA_OPTS="$JAVA_OPTS"

# Allow the container to be started with `--user`
if [[ "$1" = 'java' && "$(id -u)" = '0' ]]; then
  chown -R opcal:opcal "/app"
  exec gosu opcal "$0" "$@"
fi

SSH_PATH=/tmp/ssh
SSH_CONFIG=${SSH_PATH}/config
if [[ -d "${SSH_PATH}" && -f "${SSH_CONFIG}" ]]; then
  cp /tmp/secrets/* ~/.ssh/
  touch ~/.ssh/known_hosts
  chmod 400 ~/.ssh/*
  cp ${SSH_CONFIG} ~/.ssh/
fi

GIT_PATH=/tmp/git/
GIT_CONFIG=${GIT_PATH}/config
if [[ -d "${GIT_PATH}" && -f "${GIT_CONFIG}" ]]; then
  cp ${GIT_CONFIG} ~/.git/
fi

exec java ${JAVA_OPTS} -jar "$@" ${PARAMS}


